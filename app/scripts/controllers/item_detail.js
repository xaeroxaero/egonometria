'use strict';

/**
 * @ngdoc function
 * @name egonometriaApp.controller:ItemDetailCtrl
 * @description
 * # ItemDetailCtrl
 * Controller of the egonometriaApp
 */
angular.module('egonometriaApp')
  .controller('ItemDetailCtrl', ['$scope','$uibModal', '$currency', '$data_save', function ($scope, $uibModal, $currency, $data_save) {
    $scope.Send = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'views/send_mail.html',
        controller: 'SendMailCtrl',
        size: 'md'
      })};
    $scope.select= function(i, size) {
      $scope.selectedIndex=i;
      $scope.size = size
    };
    $scope.init = function () {
      $scope.item = $scope.$resolve.item;
      console.log($scope.item);
      $scope.currency = $data_save.getDataCurrency();
      console.log($scope.currency);
      if ($scope.currency === undefined) {
      $currency.getCurrency().then(function (data) {
        $scope.currency = data.data[10].value;
        console.log($scope.currency)
      });
      }
    };
    $scope.addToCart = function (key, value) {
      console.log(value);
      var id = value.product_id;
      var size = value.size;
      var A = $data_save.getFromStorage(key);
      var lista = A;
      var condition = false;
      if (A === null){
        $data_save.saveToStorage(key,[value]);
      }
      else {
        console.log(A);
      A.forEach(function (item) {
        console.log(item);
        if ((item.size === size) && (item.product_id === id)){
          item.quantity = item.quantity + 1;
          console.log(item.quantity);
          condition = true
        }
      });
      console.log(condition);
      if(condition === false) {
          lista.push(value);
        }
      $data_save.saveToStorage(key, lista);
      console.log(A);
      }
    };
    $scope.init();
  }]);
