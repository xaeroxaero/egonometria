'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:PaymentAdressCtrl
 * @description
 * # PaymentAdressCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('PaymentAdressCtrl', ['$scope','$data_save', function ($scope, $data_save) {
    $scope.init = function () {
      $scope.billing = $data_save.getFromStorage('billing');
      $scope.billing = $scope.billing.billing;
      $scope.first_name =$scope.billing.first_name;
      $scope.email_adress = $scope.billing.email;
      $scope.adress = $scope.billing.address_1;
      $scope.city = $scope.billing.city;
      $scope.country = $scope.billing.country;
      $scope.postal_code = $scope.billing.postcode;
      $scope.telephone_number = $scope.billing.phone;
    };


    $scope.$on('billing', function (event) {
      $data_save.saveToStorage('billing', {billing:{
        first_name: $scope.first_name,
        last_name: 'Doe',
        address_1:  $scope.adress,
        address_2: '',
        city: $scope.city,
        state: 'CA',
        postcode: $scope.postal_code,
        country: $scope.country,
        email: $scope.email_adress,
        phone: $scope.telephone_number
      }});

    });
    $scope.init()
  }]);
