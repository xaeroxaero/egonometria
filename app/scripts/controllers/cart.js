'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:CartCtrl
 * @description
 * # CartCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('CartCtrl', [ '$scope','$data_save', '$currency', '$media', '$products',
    function ($scope, $data_save, $currency, $media, $products) {
    console.log($scope.cart);
    $scope.addToCart = function (value) {
      var key = 'items';
      var id = value.product_id;
      var size = value.size;
      var A = $data_save.getFromStorage(key);
      var lista = A;
      var condition = false;
      if (A === null){
        $data_save.saveToStorage(key,[value]);
      }
      else {
        console.log(A);
        A.forEach(function (item) {
          console.log(item);
          if ((item.size === size) && (item.product_id === id)){
            item.quantity = item.quantity + 1;
            console.log(item.quantity);
            condition = true
          }
        });
        console.log(condition);
        if(condition === false) {
          lista.push(value);
        }
        $data_save.saveToStorage(key, lista);
        console.log(A);
      }
    };
    $scope.init = function () {
      $scope.cart =  $data_save.getFromStorage('items');
      $scope.currency = $data_save.getDataCurrency();
      if ($scope.currency === undefined) {
        $currency.getCurrency().then(function (data) {
          $scope.currency = data.data[10].value;
          console.log($scope.currency)
        });
      }
        var promise = $products.getProducts();
        promise.then(function (data) {
          $scope.products = data.data;
          $scope.images = [];
          $scope.products.forEach(function (image) {
            image.images.forEach(function(data){
              $scope.images.push(data.id);
            });
          });
          $media.getImage($scope.images).then(function (data) {
            $scope.image_sizes = data.data;
            $scope.products.forEach(function (image_id) {
              image_id.images.forEach(function (image) {
                function FIND(data) {
                  if (image.id === data.id) {
                    return data;
                  }
                }
                var imagez = $scope.image_sizes.find(FIND);
                if (image.id === imagez.id) {
                  image.sizes = (imagez.media_details.sizes);
                }

                $scope.cart.forEach(function (item) {
                  if (item.product_id === image_id.id){
                    item.image = image_id.images[0].sizes.thumbnail.source_url;
                    item.title = image_id.name;
                    item.price = image_id.price;
                    item.total = (item.price * item.quantity);
                  }
                }
                );
              })
            });
            var total = 0;
            $scope.cart.forEach(function (item) {
              total = total + item.total;
            });
            $scope.total = total;
          });
          $data_save.save($scope.products);
        });

    };
    $scope.init()
  }]);
