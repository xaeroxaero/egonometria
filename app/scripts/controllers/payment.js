'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:PaymentCtrl
 * @description
 * # PaymentCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('PaymentCtrl', ['$state', '$scope' ,
    function ($state, $scope) {
    $state.transitionTo('payment.adress').then(
    console.log($state.current));
    $scope.GO = function () {
      $scope.$broadcast('billing');
      if ($state.current.name === 'payment.shipment') {
        $state.transitionTo('payment.method');
        console.log($state.current);
      }
      else if ($state.current.name === 'payment.adress') {
        $state.transitionTo('payment.shipment');
        console.log($state.current)
      }
    };
    $scope.CHANGE = function (state){
      $state.transitionTo(state);
      console.log($state.current)
    };

  }]);
