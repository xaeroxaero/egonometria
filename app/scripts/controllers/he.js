'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:HeCtrl
 * @description
 * # HeCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('HeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
