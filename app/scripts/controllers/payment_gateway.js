'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:PaymentGatewayCtrl
 * @description
 * # PaymentGatewayCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('PaymentGatewayCtrl',['$scope','$payment_gateway','$data_save', function ($scope, $payment_gateway,$data_save) {
    $payment_gateway.getPaymentGateway().then(function (data) {
      $scope.payment_gateway = data.data;
      console.log($scope.payment_gateway)
    });
    $scope.select= function(i,id,title) {
      $scope.selectedIndex=i;
      Add(id,title)
    };
    function Add (id, title) {
      var key = 'payment';
      $data_save.saveToStorage(key,{
        payment_method: id,
        payment_method_title: title}
      )
    }
  }]);
