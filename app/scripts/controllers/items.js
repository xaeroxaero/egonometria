'use strict';

/**
 * @ngdoc function
 * @name egonometriaApp.controller:ShopsCtrl
 * @description
 * # ShopsCtrl
 * Controller of the egonometriaApp
 */
angular.module('egonometriaApp')
  .controller('ItemsCtrl', ['$scope', '$products', '$data_save', '$video', '$sce', '$uibModal', '$currency', '$scroll', '$media', '$order',
    function ($scope, $products, $data_save, $video, $sce, $uibModal, $currency, $scroll, $media, $order) {
      $order.postOrder();
    $scope.Image = function (id) {
      };
    $scope.API = null;
    $scope.StartVideo = function () {
      var x = document.getElementsByClassName("main-menu");
      angular.element(x[0]).addClass('hide');
    };
    $scope.onPlayerReady = function (API) {
      $scope.API = API ;
      $scope.onPlayerFinish = function () {
        var x = document.getElementsByClassName("main-menu");
        angular.element(x[0]).removeClass('hide');
      };
    };
    $scope.scroll = function (id) {
        $scroll.futureId = id
      };
    $scope.$on('scroll_on', function() {
          if($scroll.futureId){
            $scroll.Scroll($scroll.futureId);
            $scroll.futureId = null;
          }
        }
      );
      function setSingleItem(id) {
        $scope.products.forEach(function (item) {
            if (id == item.id) {
              $scope.item = item;
            }
          }
        );
      }
      $scope.Send = function () {
        var modalInstance = $uibModal.open({
          animation: false,
          templateUrl: 'views/send_mail.html',
          controller: 'SendMailCtrl',
          size: 'md'
        })
      };
      $scope.GO = function (id) {

        var modalInstance = $uibModal.open({
          animation: false,
          templateUrl: 'views/item.html',
          controller: 'ItemDetailCtrl as ctrl',
          size: 'lg',
          resolve: {
            item: function () {
              setSingleItem(id);
              return $scope.item;
            }

          }
        });
      };
      $scope.init = function () {


        $scope.videos = null;
        $scope.video = null;
        $scope.currency = null;

        $currency.getCurrency().then(function (data) {
          $scope.currency = data.data[10].value;
          $data_save.saveCurrency($scope.currency)
        });

        $scope.products = $data_save.getData();
        if ($scope.products === undefined) {
          var promise = $products.getProducts();
          promise.then(function (data) {
            $scope.$emit('scroll_now');
            $scope.products = data.data;
            // console.log($scope.products);
            $scope.images = [];
            $scope.products.forEach(function (image) {
              image.images.forEach(function(data){
                    $scope.images.push(data.id);
                  });
            });
            $media.getImage($scope.images).then(function (data) {
              $scope.image_sizes = data.data;
              console.log($scope.images);
              console.log($scope.image_sizes);
              $scope.products.forEach(function (image_id) {
                image_id.images.forEach(function (image) {
                  function FIND(data) {
                    if (image.id === data.id) {
                      return data;
                    }
                  }
                  var imagez = $scope.image_sizes.find(FIND);
                  if (image.id === imagez.id) {
                    // console.log(image);
                    image.sizes = (imagez.media_details.sizes);
                  }
                    })
              })
            });
            $data_save.save($scope.products);
            var video = 'video';
            $video.getVideo(video).then(function (data) {
              $scope.video = data.data[0].source_url;
                  $scope.config = {
                    sources: [
                      {src: $sce.trustAsResourceUrl($scope.video), type: "video/mp4"},
                      {src: $sce.trustAsResourceUrl($scope.video), type: "video/webm"},
                      {src: $sce.trustAsResourceUrl($scope.video), type: "video/ogg"}
                    ],
                    tracks: [
                      {
                        src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                        kind: "subtitles",
                        srclang: "en",
                        label: "English",
                        default: ""
                      }
                    ],
                    preload: "none",
                    autoHide: false,
                    autoHideTime: 3000,
                    autoPlay: false,
                    theme: "bower_components/videogular-themes-default/videogular.css",
                    plugins: {
                      poster: "none"
                    }
                  }
            });
          });
        }
      };

      $scope.init();
    }]);
