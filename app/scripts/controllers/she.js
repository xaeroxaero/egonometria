'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:SheCtrl
 * @description
 * # SheCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('SheCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
