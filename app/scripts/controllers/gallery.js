'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:GalleryCtrl
 * @description
 * # GalleryCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('GalleryCtrl',['$scope', '$media', function ($scope, $media) {
    $scope.gallery = [];
    $scope.init = function () {
      $media.getGallery().then( function (data) {
          $scope.images = data.data;
          console.log($scope.images);
        $scope.images.forEach(function (image) {
            console.log(image.caption.rendered);
          if (image.caption.rendered.includes('home') === true){
            $scope.gallery.push(image.media_details.sizes.large.source_url)
          }
          console.log($scope.gallery)
        }
        )

        }
      );

    };



    $scope.init()

  }]);
