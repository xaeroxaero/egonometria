'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:PaymentShipmentCtrl
 * @description
 * # PaymentShipmentCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('PaymentShipmentCtrl', ['$scope','$shipping','$data_save', function ($scope, $shipping, $data_save) {
    $shipping.getShipping().then(function (data) {
      $scope.shipping = data.data;
      console.log($scope.shipping)
    });
    $scope.select= function(i,id,title) {
      $scope.selectedIndex=i;
      Add(id, title)
    };
    function Add (id, title) {
      var key = 'shipping_lines';
      $data_save.saveToStorage(key,{shipping_lines: {
        method_id: id,
        method_title: title}}
        )
    }
  }]);

