'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('MainCtrl', ['$state', '$scope',
    function ($state, $scope) {
    $state.transitionTo('home.gallery');

    $scope.CHANGE = function (state){
        $state.transitionTo(state);
      };
  }]);
