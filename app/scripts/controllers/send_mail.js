'use strict';

/**
 * @ngdoc function
 * @name untitled1App.controller:SendMailCtrl
 * @description
 * # SendMailCtrl
 * Controller of the untitled1App
 */
angular.module('egonometriaApp')
  .controller('SendMailCtrl',['$scope', function($scope) {
    $scope.text = null;
    $scope.submit = function() {
      console.log($scope.text);
      if ($scope.text) {
        $scope.text = '';
      }
    };
  }]);
