'use strict';

/**
 * @ngdoc directive
 * @name untitled1App.directive:path
 * @description
 * # path
 */
angular.module('egonometriaApp')
  .directive('bgImage', function () {
  return {
    link: function(scope, element, attr) {
      attr.$observe('bgImage', function() {
        if (!attr.bgImage) {
          return;
        }
        attr.bgImage = JSON.parse(attr.bgImage);
          var image = new Image();
          var large = attr.bgImage.large ? attr.bgImage.large.source_url : null;
          var full = attr.bgImage.full.source_url;
          image.src = attr.bgImage;
          if (large === null) {
            image.onerror = function () {
              //Image loaded- set the background image to it
              element.css("background-image", "url(" + full + ")");
            };
          }
          else {
            image.onerror = function () {
              //Image failed to load- use default
              element.css("background-image", "url(" + large + ")");
            };
          }
      });
    }
  };
});
