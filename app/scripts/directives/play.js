'use strict';

/**
 * @ngdoc directive
 * @name untitled1App.directive:play
 * @description
 * # play
 */
angular.module('egonometriaApp')
  .directive('play', function () {
    return {
      restrict: 'A',
      link: function (scope, element) {
        element.bind('click', function (e) {
          var x = document.getElementsByClassName("iconButton");
          angular.element(x[0]).trigger('click');
          console.log(x[0]);

        });
      }
    };
  });
