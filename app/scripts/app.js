'use strict';

/**
 * @ngdoc overview
 * @name untitled1App
 * @description
 * # untitled1App
 *
 * Main module of the application.
 */
angular
  .module('egonometriaApp', [
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    "ngSanitize",
    "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster",
    'duScroll',
    'base64',
    'LocalStorageModule'
  ])
  .value('duScrollBottomSpy', true)
  .value('duScrollOffset', 0)
  .value('duScrollDuration', 1800)
  .value('duScrollCancelOnEvents', false)
  .run([
    "$rootScope", "$state", "$stateParams", function ($rootScope, $state, $stateParams) {
      $rootScope.$state = $state;
      return $rootScope.$stateParams = $stateParams;
    }
  ])
  .config(function ($stateProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
      .state('home', {
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'MainCtrl'
      })
      .state('home.all', {
        url: 'all',
        templateUrl: 'views/items.html',
        controller: 'ItemsCtrl'
      })
      .state('home.gallery', {
        url: 'gallery',
        templateUrl: 'views/gallery.html',
        controller: 'GalleryCtrl'
      })
      .state('home.she', {
        url: 'gallery',
        templateUrl: 'views/she.html',
        controller: 'SheCtrl'
      })
      .state('home.he', {
        url: 'gallery',
        templateUrl: 'views/he.html',
        controller: 'HeCtrl'
      })
      .state('cart',
        {
          url: '/cart',
          controller: 'CartCtrl',
          templateUrl: 'views/cart.html'
        })
      .state('payment',
        {
          url: '/payment',
          controller: 'PaymentCtrl',
          templateUrl: 'views/payment.html'
        })
      .state('payment.adress', {
        templateUrl: 'views/payment.adress.html',
        controller: 'PaymentAdressCtrl'
      })
      .state('payment.shipment', {
        templateUrl: 'views/payment.shipment.html',
        controller: 'PaymentShipmentCtrl'
      })
      .state('payment.method', {
        templateUrl: 'views/payment.method.html',
        controller: 'PaymentGatewayCtrl'
      })

  })
  .config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('egonometriaApp')
      .setStorageType('localStorage')
      .setNotify(true, true)
  });

