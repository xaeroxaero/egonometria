'use strict';

/**
 * @ngdoc filter
 * @name untitled1App.filter:htmlTag
 * @function
 * @description
 * # htmlTag
 * Filter in the untitled1App.
 */
angular.module('egonometriaApp')
  .filter('htmlTag', function () {
      return function(text) {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
      };
    }
  );
