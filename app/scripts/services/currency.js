'use strict';

/**
 * @ngdoc service
 * @name egonometriaApp.products
 * @description
 * # products
 * Service in the egonometriaApp.
 */
angular.module('egonometriaApp')
  .service('$currency', ['$http', '$base64', function ($http, $base64) {
   var consumer_key = "ck_cf30c19c656b7efb34e9ba1d3bf90d2a476aca48",
      httpMethod = "GET",
      url = "https://it-grafika.pl:8080/wp-json/wc/v2/settings/general",
      consumerSecret = "cs_8aebc5b58ada7e6f37d99c0565165b83478738f3";
    var cs = $base64.encode(consumer_key +':' + consumerSecret);
    this.getCurrency = function () {
      return $http({
        method: httpMethod,
        url: url,
        headers: {
          "authorization": "Basic "+ cs
        }
      })
        .then(function (data) {
          return data;
        });
    };
  }])
;
