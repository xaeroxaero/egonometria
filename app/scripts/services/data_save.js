'use strict';

/**
 * @ngdoc service
 * @name egonometriaApp.dataSave
 * @description
 * # dataSave
 * Service in the egonometriaApp.
 */
angular.module('egonometriaApp')
  .service('$data_save',['localStorageService', function (localStorageService) {
      this.save = function (products) {
        this.products = products;
      };
    this.saveCurrency = function (currency) {
      this.currency = currency;
    };
      this.getData = function () {
        return this.products

      };
    this.getDataCurrency = function () {
      return this.currency
    };

      this.saveToStorage = function (key, value) {
        return localStorageService.set(key, value);
      };

      this.getFromStorage = function (key) {
        console.log(localStorageService.keys());
        console.log(localStorageService.get('billing'));
        return localStorageService.get(key);
      }
    }
  ]);
