'use strict';

/**
 * @ngdoc service
 * @name untitled1App.media
 * @description
 * # media
 * Service in the untitled1App.
 */
angular.module('egonometriaApp')
  .service('$media',  ['$http', function ($http) {
    this.getImage = function (id) {
      return $http.get('https://it-grafika.pl:8080/wp-json/wp/v2/media?per_page=100&include=' + id);
    };
    this.getGallery = function () {
      return $http.get('https://it-grafika.pl:8080/wp-json/wp/v2/media?per_page=100')
    }
  }])
;
