'use strict';

/**
 * @ngdoc service
 * @name untitled1App.scroll
 * @description
 * # scroll
 * Service in the untitled1App.
 */
angular.module('egonometriaApp')
  .service('$scroll',
    ['$document',
      function ($document) {
        this.Scroll = function (id) {
          setTimeout(function () {
              var someElement = angular.element(document.getElementById(id));
              $document.duScrollToElementAnimated(someElement)
            }, 100
          );
        };
      }]
  );
