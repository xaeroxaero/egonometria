'use strict';

/**
 * @ngdoc service
 * @name untitled1App.order
 * @description
 * # order
 * Service in the untitled1App.
 */
angular.module('egonometriaApp')
  .service('$order', ['$http', '$base64', function ($http, $base64) {
    var order={
      payment_method: 'bacs',
      payment_method_title: 'Direct Bank Transfer',
      set_paid: true,
      billing: {
        first_name: 'John',
        last_name: 'Doe',
        address_1: '969 Market',
        address_2: '',
        city: 'San Francisco',
        state: 'CA',
        postcode: '94103',
        country: 'US',
        email: 'john.doe@example.com',
        phone: '(555) 555-5555'
      },
      shipping: {
        first_name: 'John',
        last_name: 'Doe',
        address_1: '969 Market',
        address_2: '',
        city: 'San Francisco',
        state: 'CA',
        postcode: '94103',
        country: 'US'
      },
      line_items: [
        {
          product_id: 93,
          quantity: 2
        },
        {
          product_id: 22,
          variation_id: 23,
          quantity: 1
        }
      ],
      shipping_lines: [
        {
          method_id: 'flat_rate',
          method_title: 'Flat Rate',
          total: 10
        }
      ]
    };

    var consumer_key = "ck_cf30c19c656b7efb34e9ba1d3bf90d2a476aca48",
      httpMethod = "POST",
      url = "https://it-grafika.pl:8080/wp-json/wc/v2/orders",
      consumerSecret = "cs_8aebc5b58ada7e6f37d99c0565165b83478738f3";
    var cs = $base64.encode(consumer_key +':' + consumerSecret);
    this.postOrder = function () {
      var config = {
        method: httpMethod,
        url: url,
        "headers": {
          "Authorization": "Basic "+ cs,
          "Content-Type": "application/json"
        },

        data: order
      };
      console.log($http(config)
        .then(function (data) {
          console.log(data);
          return data;
        }));
    };
  }])
;
