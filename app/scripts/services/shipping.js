'use strict';

/**
 * @ngdoc service
 * @name untitled1App.shipping
 * @description
 * # shipping
 * Service in the untitled1App.
 */
angular.module('egonometriaApp')
  .service('$shipping',  ['$http', '$base64', function ($http, $base64) {
    var consumer_key = "ck_cf30c19c656b7efb34e9ba1d3bf90d2a476aca48",
      httpMethod = "GET",
      url = "https://it-grafika.pl:8080/wp-json/wc/v2/shipping/zones/1/methods",
      consumerSecret = "cs_8aebc5b58ada7e6f37d99c0565165b83478738f3";
    var cs = $base64.encode(consumer_key +':' + consumerSecret);
    this.getShipping = function () {
      return $http({
        method: httpMethod,
        url: url,
        headers: {
          "authorization": "Basic "+ cs
        }
      })
        .then(function (data) {
          return data;
        });
    };
  }])
;
