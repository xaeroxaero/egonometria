'use strict';
/**
 * @ngdoc service
 * @name egonometriaApp.products
 * @description
 * # products
 * Service in the egonometriaApp.
 */
angular.module('egonometriaApp')
  .service('$video', ['$http', function ($http) {
    this.getVideo = function (video) {
      return $http.get('https://it-grafika.pl:8080/wp-json/wp/v2/media?search='+video);
    };
  }])
;
